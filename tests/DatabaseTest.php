<?php

declare(strict_types=1);

namespace NORA\Database;

use PHPUnit\Framework\TestCase;

class DatabaseTest extends TestCase
{
    protected Database $database;

    protected function setUp(): void
    {
        $this->database = new Database();
    }

    public function testIsInstanceOfDatabase(): void
    {
        $actual = $this->database;
        $this->assertInstanceOf(Database::class, $actual);
    }
}
