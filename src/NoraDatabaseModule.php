<?php

namespace NORA\Database;

use Ray\Di\AbstractModule;

class NoraDatabaseModule extends AbstractModule
{
    public function __construct(
        private array $options
    ) {
    }

    public function configure()
    {
        $this->bind()->annotatedWith('database_configs')->toInstance($this->options);
        if (isset($this->options['default'])) {
            $this
                ->bind(NoraDatabaseClient::class)
                ->toProvider(NoraDatabaseProvider::class, 'default');
        }
        foreach (array_keys($this->options) as $name) {
            $this
                ->bind(NoraDatabaseClient::class)
                ->annotatedWith($name)
                ->toProvider(NoraDatabaseProvider::class, $name);
        }
    }
}
