<?php

namespace NORA\Database;

use Ray\Di\Di\Named;
use Ray\Di\ProviderInterface;
use Ray\Di\SetContextInterface;

class NoraDatabaseProvider implements ProviderInterface, SetContextInterface
{
    private $configs;

    public function setContext($context)
    {
        $this->context = $context;
    }

    public function __construct(#[Named('database_configs')] array $configs)
    {
        $this->configs = $configs;
    }

    public function get(): NoraDatabaseClient
    {
        $config = $this->configs[$this->context];

        $client = new NoraDatabaseClient(
            host: $config['host'],
            port: $config['port'] ?? 3306,
            database: $config['database'],
            user: $config['user'],
            password: $config['password']
        );

        $client->getPdo()->exec('SET time_zone="Asia/Tokyo"');

        return $client;
    }
}
