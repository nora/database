<?php

namespace NORA\Database;

use Envms\FluentPDO\Queries\Delete;
use PDO;
// https://github.com/envms/fluentpdo
use \Envms\FluentPDO\Query;
use \Envms\FluentPDO\Queries\Select;
use \Envms\FluentPDO\Queries\Insert;
use Envms\FluentPDO\Queries\Update;
use PDOStatement;

class NoraDatabaseClient
{
    private Query $query;
    private PDO $pdo;

    public function __construct(private string $host, private string $database, private string $user, private string $password, private int $port = 3306)
    {
        $this->query = $this->connect();
    }

    public function getPdo(): PDO
    {
        return $this->pdo;
    }

    public function connect(): Query
    {
        $this->pdo = new PDO(
            sprintf(
                'mysql:host=%s;port=%s;dbname=%s',
                $this->host,
                $this->port,
                $this->database
            ),
            $this->user,
            $this->password
        );

        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = new Query($this->pdo);
        $query->convertWriteTypes(true);
        return $query;
    }

    public function refresh(): void
    {
        unset($this->pdo);
        unset($this->query);
        $this->query = $this->connect();
    }

    public function query(string $sql): PDOStatement
    {
        return $this->query->getPDO()->query($sql);
    }

    public function prepare(string $sql): PDOStatement
    {
        return $this->query->getPDO()->prepare($sql);
    }

    public function from(string $table): Select
    {
        return $this->query->from($table);
    }

    public function insertInto(string $table): Insert
    {
        return $this->query->insertInto($table);
    }

    public function update(string $table): Update
    {
        return $this->query->update($table);
    }

    public function delete(string $table): Delete
    {
        return $this->query->delete($table);
    }
}
